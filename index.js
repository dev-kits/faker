const Yod = require('yod-mock');
const ObjectsToCsv = require('objects-to-csv')

/*
    兰州市         兰州健康码  620100
    嘉峪关市       嘉峪关健康码 620200
    金昌市         金昌健康码  620300
    白银市         白银健康码  620400
    天水市         天水健康码  620500
    武威市         武威健康码  620600
    张掖市         张掖健康码  620700
    平凉市         平凉健康码  620800
    酒泉市         酒泉健康码  620900
    庆阳市         庆阳健康码  621000
    定西市         定西健康码  621100
    陇南市         陇南健康码  621200
    临夏回族自治州  临夏健康码  622900
    甘南藏族自治州  甘南健康码  623000
*/
Yod.type('Health', {
    "allInCity": 0,
    "isContact": 1,
    "isLeaveCity": 0,
    "isContactSuspected": 0,
    "currentHealth": [{ "key": "dry_cough" }]
});

Yod.type('User', {
    name: '@ChineseName',
    idcardType: 'IDENTITY_CARD',
    idcardNo: '@Int(622000000000000000 , 622999999999999999)',
    cityNo: '@Int(620000, 623000)',
    phone: '@Tel',
    source: 'alipay',
    currentLocation: '@ChineseName, @CW@ChineseName',
    realtimeLocation: '@ChineseName, @CW@ChineseName',
    hasBeanTo: '["@Self.currentLocation", "@Self.realtimeLocation"]',
    health: '@Health',
    agentIdcardType: 'IDENTITY_CARD',
    agentIdcardNo: '@Int(600000000000000000 , 699999999999999999)'
});

async function printCsv(data) {
    console.log(
        await new ObjectsToCsv(data).toString()
    );
}

const dat = Yod('@User.repeat(2)');
printCsv(dat);

async function saveCsv(data) {
    const opts = { append: true, bom: true };
    await new ObjectsToCsv(data).toDisk('./data.csv', opts);
    console.log('Save file done.');
}

console.log('creating.....');
for(let i = 0; i < 10; i++) {
    //saveCsv(Yod('@User.repeat(1000)'));
}
